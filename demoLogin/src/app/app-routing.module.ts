import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateAccountComponent } from './create-account/create-account.component';
import { LoginComponentComponent } from './login-component/login-component.component';

import { ReportComponent } from './report/report.component';
import { RouteGuardService } from './service/auth.guard';
import { TaoHoaDonComponent } from './tao-hoa-don/tao-hoa-don.component';
import { ThemNhanVienComponent } from './them-nhan-vien/them-nhan-vien.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: "login", component: LoginComponentComponent },
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "welcome", component: WelcomeComponent, canActivate: [RouteGuardService], children:
      [{ path: "them-nhan-vien", component: ThemNhanVienComponent },
      { path: "create-acc", component: CreateAccountComponent },
      { path: "tao-hoa-don", component: TaoHoaDonComponent },
      { path: "report", component: ReportComponent }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
