import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  formLogin: FormGroup;
  errorMessage:string = null;
  
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = new FormGroup(
      {
        'username': new FormControl(null, [Validators.required, Validators.email]), 
        'password': new FormControl(null, [Validators.required])
      }
    );
  }

  onSubmit(){
    this.authService.logIn(this.formLogin.value['username'], this.formLogin.value['password']).subscribe(
        resData=>{
            this.router.navigate(['/welcome']);
        }, 
        errorMessage=>{
            this.errorMessage = errorMessage;
        }
    );
  }
  
}