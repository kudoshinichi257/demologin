import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  role: string = null;
  isOwner: boolean = false;
  isKeToan: boolean = false;
  isThuNgan: boolean = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    // this.authService.user.subscribe(user => {
    //   if(user.getRole() == 'quan ly'){
    //     this.isOwner = true;
    //   }
    //   if(user.getRole() == 'thu ngan'){
    //     this.isThuNgan = true;
    //   }else{
    //     this.isKeToan = true;
    //   }
    // })
  }

  logOut(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
