import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../model/user.model';
import { Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Subject<User>=new Subject();

  constructor(private http: HttpClient) { }

  logIn(username: string, password: string) {
    return this.http.post<User>('http://ec2-13-58-100-139.us-east-2.compute.amazonaws.com:8888/api/user/login',
      {
        accUsername: username,
        accPassword: password
      }
    ).pipe(catchError(errorObject => {
        switch(errorObject.error.error){
          case 'Not Found':
            return throwError('User not found');
            default:
            return throwError('An unknown error occured');;
           
        }
        
    }), tap(
      resData => {
        this.user.next(resData);
        localStorage.setItem('userData', JSON.stringify(resData));
      }
    )
    )
  }

  autoLogin(){
    const userData: User = JSON.parse(localStorage.getItem('userData'));
    if(!userData)
    {
      return;
    }
    this.user.next(userData);
  }

}
