export class User{
    constructor(private username: string, private  role: {a: string, b:string}[]){}

    getUsername(){
        return this.username;
    }

    getRole(){
        return this.role;
    }
    
}