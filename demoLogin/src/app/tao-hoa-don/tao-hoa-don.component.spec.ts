import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaoHoaDonComponent } from './tao-hoa-don.component';

describe('TaoHoaDonComponent', () => {
  let component: TaoHoaDonComponent;
  let fixture: ComponentFixture<TaoHoaDonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaoHoaDonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaoHoaDonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
